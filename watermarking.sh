echo "Airsoft Moments Watermarking Script v1.0.0"

name=$1
nick=$2
input=$3

if ((${#input} == 0)); then
	input="in"
fi

cd "$input"
mkdir -p watermarked

for image in *jpg *JPG *jpeg *JPEG
do
 if [ -s $image ] ; then   # non-zero file size
    width=$(identify -format %w $image)
    height=$(identify -format %h $image)
	orientation=$(identify -quiet -format '%[EXIF:Orientation]' $image)

	if [ ! $orientation ]; then
    	orientation=1
	fi
	
	realWidth=$((orientation == 1 ? $width : $height))
	realHeight=$((orientation == 1 ? $height : $width))
	barHeight=$((realWidth < realHeight ? realWidth / 33 : realHeight / 33))
	
	dim=2048
	max=$((width > height ? width : height))
	if (($max < 2048)); then
		dim=$max
	fi
	
    convert -auto-orient -resize ${dim}x${dim} -background '#0005' -fill '#fffb' -gravity center -font Arial \
       -size ${width}x${barHeight} caption:"© ${name} - ${nick}@airsoftmoments.com" \
       $image -define jpeg:extent=1024kb +swap -gravity South -composite watermarked/$image

	echo "watermarked $image, orientation=${orientation}, dimensions=${realWidth}x${realHeight}"
  fi
done
cd .. 