echo "Airsoft Moments Watermarking Script v1.1.0"

source ./watermark.properties
echo "Hi $name, $nick@airsoftmoments.com"

input="input"

cd "$input"

unixfind -type f -name '*.jpg' -or -name '*.JPG' -or -name '*.jpeg' -or -name '*.JPEG' | while read image
do
 if [ -s "$image" ] ; then   # non-zero file size
    width=$(identify -format %w "$image")
    height=$(identify -format %h "$image")
	orientation=$(identify -quiet -format '%[EXIF:Orientation]' "$image")

	if [ ! $orientation ]; then
    	orientation=1
	fi
	
	realWidth=$((orientation == 1 ? $width : $height))
	realHeight=$((orientation == 1 ? $height : $width))
	barHeight=$((realWidth < realHeight ? realWidth / $heightDivider : realHeight / $heightDivider))
	
	dim=2048
	max=$((width > height ? width : height))
	if (($max < 2048)); then
		dim=$max
	fi
	
	imageDir=$(dirname "$image")
	mkdir -p ../output/"$imageDir"
	
    "..\bin\imagemagick\convert.exe" -auto-orient -resize ${dim}x${dim} -background '#0005' -fill '#fffb' -gravity center -font Arial \
       -size ${width}x${barHeight} caption:"© ${name} - ${nick}@airsoftmoments.com" \
       "$image" -define jpeg:extent=$fileSize +swap -gravity South -composite "../output/$image"

	echo "watermarked $image, orientation=${orientation}, dimensions=${realWidth}x${realHeight}"
  fi
done
cd .. 
echo "Watermarking done! Check the output folder."